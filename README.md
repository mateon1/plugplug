A plug.dj JavaScript plugin.

Get the latest version by bookmarking the code below, and clicking it every time you load Plug
```
#!javascript

javascript:void($.getScript("//plug.unnoen.com/plugplug/"));
```

~~Alternatively, you can use [this userscript](http://userscripts.org:8080/scripts/show/396065) to have PlugPlug automatically load every time you load Plug.DJ. You will need some Userscript extension such as Greasemonkey or Tampermonkey.~~ Userscripts.org is down, I wasn't able to update the script, just use the bookmark for now.

** REPORT BUGS TO THE 'ISSUES' TAB **
